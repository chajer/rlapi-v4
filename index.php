<?php

    require 'vendor/autoload.php';

    use \Ratelimited\Api\Controllers\Users as Users;
    // use \Ratelimited\Api\Controllers\ApiKeys as ApiKeys;
    // use \Ratelimited\Api\Controllers\UploadKeys as UploadKeys;

    use \Ratelimited\Api\Router\Request as Request;
    use \Ratelimited\Api\Router\Response as Response;

    $dotenv = Dotenv\Dotenv::create(__DIR__);
    $dotenv->load();

    \Sentry\init(['dsn' => getenv('SENTRY_DSN')]);

    $router = new \AltoRouter();

    /**
     * 
     * Define routes here
     * 
     * @see https://altorouter.com/usage/mapping-routes.html
     */

    $router->map( 'GET', '/', function()
    {
        // This can be requested as anything, as it does not care for user input.

        return new Response(json_encode([
            'instance_info' => [
                'version' => 'v4-indev'
            ]
        ]));
    });

    /**
     * User Routes
     */

    /**
     * Creates a user
     * 
     * @param string $_POST['username']
     * @param string $_POST['email']
     * @param string $_POST['password']
     * 
     * @return Response json'd from Controller
     */
    $router->map('POST', '/users/create', function()
    {
        // Init variables
        $request = new Request();
        $_POST = $request->getJsonBody();
        $opts = []; // initialize empty options array

        if($request->isJson())
        {
            if(array_key_exists('username', $_POST) && array_key_exists('email', $_POST) && array_key_exists('password', $_POST)){
                if(array_key_exists('bypass_pp', $_POST) && $_POST['bypass_pp'] == true){ // if the user wants to bypass Pwned Passwords
                    $opts = [
                        'bypass_pwnedPasswords' => true // set the option bypass_PwnedPasswords to true.
                    ];
                } else { // else 
                    $opts = [
                        'bypass_pwnedPasswords' => false // set it to false
                    ];
                }

                $users = new Users();
                $create_user = $users->create($_POST['username'], $_POST['email'], $_POST['password'], $opts);

                return new Response(json_encode($create_user));
            } else { // If missing username, email, or password
                return new Response(json_encode([
                    'success' => false,
                    'error_msg' => 'Missing one of the required POST body parameters (Note, must be in JSON form, not form data)',
                    'required_params' => ['username', 'email', 'password']
                ]), 400);
            }
        } else {
            return new Response(json_encode([
                'success' => false,
                'error_msg' => 'content-type must be of application/json'
            ]), 400);
        }
    });

    /**
     * Verify a user's email address
     * 
     * @param string verification_id
     * 
     * @return string JSON
     */

     $router->map('GET', '/user/verify/[*:verification_id]', function($verification_id)
     {
        $request = new Request();
        $users = new Users();

        $verification = $users->verify($verification_id);
        return new Response(json_encode($verification));
     });

    /**
     * Get current user
     *
     * @param header: Authorization
     *
     * @return string JSON
     */
    $router->map('GET', '/user', function()
    {
        $request = new Request();
        $users = new Users();
        if($request->isJson()){
            $authorization = $request->getHeader('Authorization');

            if(!empty($authorization)){ // @todo: verify user exists. (also, look down)
                $get_user = $users->getOne($authorization); // @todo: When api keys and session tokens are available, get user id from api key or session token
                return new Response(json_encode($get_user));
            } else {
                return new Response(json_encode([
                    'success' => false,
                    'error_msg' => 'Missing the Authorization header.'
                ]), 401);
            }
        } else {
            return new Response(json_encode([
                'success' => false,
                'error_msg' => 'content-type must be of application/json'
            ]), 400);
        }
    });

    /**
     * 
     * Matching process
     * 
     * @see https://altorouter.com/usage/matching-requests.html
     * @see https://altorouter.com/usage/processing-requests.html
     */

    // match current request url
    $match = $router->match();

    // Register URL query string parameters in $_GET since Altorouter ROUTE doesn't deal with these.
    $parts = parse_url($_SERVER['REQUEST_URI']);
    if (isset($parts['query'])) {
        parse_str($parts['query'], $_GET);
    }

    // call closure or throw 404 status
    if( is_array($match) && is_callable( $match['target'] ) ) {
        call_user_func_array( $match['target'], $match['params'] );
    } else {
        // no route was matched
        header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');

        return new Response(json_encode([
            'error_msg' => 'route not found'
        ]));
    }
