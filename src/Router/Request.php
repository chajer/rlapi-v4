<?php
    namespace Ratelimited\Api\Router;

    class Request {
        public function getHeaders(): array
        {
            $headers = [];
            foreach ($_SERVER as $key => $value) {
                $headers[str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))))] = $value;
            }
            return $headers;
        }

        public function getContentType(): string
        {
            return $this->getHeader('Content-Type');
        }

        public function getHeader(string $header)
        {
             $headers = $this->getHeaders();
             if(array_key_exists($header, $headers))
             {
                 return $headers[$header];
             } else {
                 $headers[$header] = null;
                 return $headers[$header];
             }
        }

        public function isJson(): bool
        {
            if($this->getContentType() === 'application/json')
            {
                return true;
            } else {
                return false;
            }
        }

        public function getJsonBody(): array
        {
            return json_decode(file_get_contents('php://input'), true);
        }
    }
