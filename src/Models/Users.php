<?php
    namespace Ratelimited\Api\Models;
    use Ramsey\Uuid\Uuid;
    use Ratelimited\Api\Utils\SecurityUtils as Security;
    use Ratelimited\Api\Utils\Mailer;

    class Users {
        
        private $dbconn;
        private $security;

        public function __construct()
        {
            $this->dbconn = pg_connect('host='.getenv('DB_HOST').' port=5432 dbname='.getenv('DB_NAME').' user='.getenv('DB_USERNAME').' password='.getenv('DB_PASSWORD'));
            $this->security = new Security;
        }

        /**
         * Creates a User.
         * 
         * @param string $username
         * @param string $email
         * @param string $password
         * 
         * @return array
         */
        public function create(string $username, string $email, string $password, $opts=null)
        {
            $userID = Uuid::uuid4();
            $userID = $userID->toString();

            if($this->emailIsUnique($email) && $this->usernameIsUnique($username))
            {
                if($this->security->passwordNotPwned($password) || $opts['bypass_pwnedPasswords'] == true)
                { // if password is not pwned OR bypassed
                    $password = password_hash($password, PASSWORD_DEFAULT);

                    if(getenv('SQREEN_ENABLED'))
                    {
                        $this->security->sqreen_newUser($username);
                    }

                    try { // Begin try-catch for db-write
                        pg_prepare($this->dbconn, 'create_user', 'INSERT INTO users (id, username, email, password, tier, verified, is_blocked, is_admin) VALUES ($1, $2, $3, $4, \'free\', false, false, false)');
                        $user_creation_exec = pg_execute($this->dbconn, 'create_user', [$userID, $username, $email, $password]);

                        if(!$user_creation_exec)
                        {
                            throw new \Exception('User Creation: Writing to database failed with error: '.pg_last_error());
                        }
                    } catch(\Exception $exception) {
                        \Sentry\captureException($exception);

                        http_response_code(500); // internal server error
                        return [
                            'success' => false,
                            'error_msg' => 'Something went wrong! Please let an instance administrator know.'
                        ];
                    }

                    $mailer = new Mailer();
                    $mailer->sendVerificationEmail($username, $email);
                    
                    return [
                        'success' => true,
                        'status' => 'created',
                        'account' => [
                            'id' => $userID,
                            'username' => $username,
                            'email' => $email
                        ],
                        'message' => 'Great! Please check your e-mail in order to verify your email!'
                    ];
                } else { // password pwned & not bypassed
                    http_response_code(400); // bad request
                    return [
                        'success' => false,
                        'error_msg' => 'Your password has been pwned before (checked with HaveIBeenPwned\'s PwnedPasswords), are you sure you want to continue? If so, use bypass_pp: true in your request.',
                        'more_info' => 'https://haveibeenpwned.com/Passwords'
                    ];
                }
            } else { // Email or username already used
                http_response_code(400);
                return [
                    'success' => false,
                    'error_msg' => 'This email or username has already been used with the service. Please use a different email or username.'
                ];
            }
        }

        /**
         * Verify a user's email
         * 
         * @param string $verification_id
         * 
         * @return array
         */
        public function verify($verification_id)
        {
            pg_prepare($this->dbconn, 'get_verification', 'SELECT * FROM verifications WHERE id = $1');
            $exec       =   pg_execute($this->dbconn, 'get_verification', [$verification_id]);
            $result     =   pg_fetch_array($exec);

            $user_id    =   $result['user_id'];
            $used       =   $result['used'];
            $expiry     =   $result['expiration'];

            if(time() <= $expiry && $used == false){
                try {
                    pg_prepare($this->dbconn, 'verify_user', 'UPDATE users SET verified = true WHERE id = $1');
                    pg_prepare($this->dbconn, 'mark_verification_as_used', 'UPDATE verifications SET used = true WHERE id = $1');

                    $exec_verify_user = pg_execute($this->dbconn, 'verify_user', [$user_id]);
                    $exec_mark_verification_as_used = pg_execute($this->dbconn, 'mark_verification_as_used', [$verification_id]);

                    if(!$exec_verify_user || !$exec_mark_verification_as_used)
                    {
                        throw new \Exception('User Verification: Writing to database failed with error: '.pg_last_error());
                    }
                } catch(\Exception $exception) {
                    \Sentry\captureException($exception);

                    http_response_code(500); // internal server error
                    return [
                        'success' => false,
                        'error_msg' => 'Something went wrong! Please let an instance administrator know.'
                    ];
                }

                return [
                    'success' => true
                ];
            } else {
                return [
                    'success' => false,
                    'error_msg' => 'This verification link has been used or has expired.'
                ];
            }
        }

        /* Getter Functions */

        /**
         * Gets all users in a non-identifiying manner.
         * 
         * @return array [id, tier, is_blocked]
         */
        public function getAll()
        {
            return pg_fetch_all(
                pg_exec(
                    $this->dbconn,
                    'SELECT id, tier, is_blocked FROM users'  // Playing it safe for now
                )
            );
        }

        /**
         * Gets one user based on the user ID given.
         * 
         * @param string $userID
         * 
         * @return array
         */
        public function getOne(String $userID)
        {
            pg_prepare($this->dbconn, 'get_one_user', 'SELECT id, tier, is_blocked FROM users WHERE id = $1');
            return pg_fetch_all(pg_execute($this->dbconn, 'get_one_user', [$userID]));
        }

        public function getBilling(string $userID)
        {
            pg_prepare($this->dbconn, 'get_billing', 'SELECT tier FROM users WHERE id = $1');
            $tier = pg_fetch_array(pg_execute($this->dbconn, 'get_billing', [$userID]));
            $tier = $tier['tier'];

            return [
                $userID => [ // Begin the array with the user object
                    'plan' => $tier,
                    'transactions' => [
                        // @todo: get transactions for user
                    ]
                ]
            ];
        }

        public function generateVerificationLink(string $email) {
            pg_prepare($this->dbconn, 'get_userId_from_email', 'SELECT id FROM users WHERE email = $1');
            $exec               =   pg_execute($this->dbconn, 'get_userId_from_email', [$email]);
            $user_id            =   pg_fetch_array($exec);
            $user_id            =   $user_id['id'];

            $verification_id    =   Uuid::uuid4();
            $verification_id    =   $verification_id->toString();
            $expiry             =   time() + (3600*24);

            try {
                pg_prepare($this->dbconn, 'insert_verification_link', 'INSERT INTO verifications (id, user_id, used, expiration) VALUES ($1, $2, false, $3)');
                $exec = pg_execute($this->dbconn, 'insert_verification_link', [$verification_id, $user_id, $expiry]); // Insert the verification ID with an expiration of 24 hours.
                if(!$exec)
                {
                    throw new \Exception('Verification Link Creation: Writing to database failed with error: '.pg_last_error());
                }
            } catch(\Exception $exception) {
                \Sentry\captureException($exception);

                http_response_code(500); // internal server error
                return [
                    'success' => false,
                    'error_msg' => 'Something went wrong! Please let an instance administrator know.'
                ];
            }

            return $verification_id;
        }

        /* Utility functions */


        /**
         * Get email for user
         *
         * @param string $userID
         *
         * @return string
         */
        private function getEmailForUser(string $userID)
        {
            pg_prepare($this->dbconn, 'get_email_for_user', 'SELECT email FROM users WHERE id = $1');
            $email = pg_fetch_array(pg_execute($this->dbconn, 'get_email_for_user', [$userID]));

            return $email['email'];
        }

        /**
         * Checks the email provided against the database count to see if it is unique.
         * 
         * @param string $email
         * 
         * @return bool
         */
        private function emailIsUnique(string $email)
        {
            pg_prepare($this->dbconn, 'is_email_unique', 'SELECT COUNT(*) FROM users WHERE email = $1');
            $count = pg_fetch_array(pg_execute($this->dbconn, 'is_email_unique', [$email]));

            if($count['count'] >= 1){
                return false;
            } else {
                return true;
            }
        }

        /** Checks the username provided against the database count to see if it is unique
         * 
         * @param string $username
         * 
         * @return bool
         */
        private function usernameIsUnique(string $username)
        {
            pg_prepare($this->dbconn, 'is_username_unique', 'SELECT COUNT(*) FROM users WHERE username = $1');
            $count = pg_fetch_array(pg_execute($this->dbconn, 'is_username_unique', [$username]));

            if($count['count'] >= 1){
                return false;
            } else {
                return true;
            }
        }
    }
