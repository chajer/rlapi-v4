<?php
    namespace Ratelimited\Api\Utils;

    class SecurityUtils {

        private $dbconn;

        public function __construct()
        {
            $this->dbconn = pg_connect('host='.getenv('DB_HOST').' port=5432 dbname='.getenv('DB_NAME').' user='.getenv('DB_USERNAME').' password='.getenv('DB_PASSWORD'));
        }

        /**
         * Validates if a given UUID is valid.
         * 
         * @param string $uuid
         * 
         * @return bool
         */
        public function isValidUUID($uuid)
        {
            // @author: https://stackoverflow.com/a/19989922
            $UUIDv4 = '/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i';
            if(preg_match($UUIDv4, $uuid)){
                return true;
            } else {
                return false;
            }
        }
        
        /**
         * Checks a password against the PwnedPasswords service.
         * 
         * @param string $password
         * 
         * @return bool
         */
        public function passwordNotPwned(string $password)
        {
            $pwnedPasswords = new \PwnedPasswords\PwnedPasswords;

            $is_pwned = $pwnedPasswords->isPwned($password);
            if($is_pwned)
            {
                return false;
            } else {
                return true;
            }
        }

        /**
         * Logs a signup in Sqreen (The security automation provider that RATELIMITED uses).
         * 
         * @param string $username
         * 
         * @see https://sqreen.com | Note: This might throw linting errors in your environment. Sqreen is a php extension. It works in the official RATELIMITED environment.
         */
        public function sqreen_newUser(string $username)
        {
            \sqreen\signup_track(['username' => $username]);
        }
    }
