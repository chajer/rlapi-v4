<?php
    namespace Ratelimited\Api\Utils;

    class AuthUtils {

        private $dbconn;

        public function __construct()
        {
            $this->dbconn = pg_connect('host='.getenv('DB_HOST').' port=5432 dbname='.getenv('DB_NAME').' user='.getenv('DB_USERNAME').' password='.getenv('DB_PASSWORD'));
        }

        /**
         * Verify that the password provided by the user matches the one in the database.
         * 
         * @param string $userID
         * @param string $password
         * 
         * @return bool
         */
        public function verify_password(string $userID, string $password)
        {
            pg_prepare($this->dbconn, 'get_password_hash', 'SELECT password FROM users WHERE id = $1');
            $password_hash = pg_fetch_array(pg_execute($this->dbconn, 'get_password_hash', [$userID]));
            if(password_verify($password, $password_hash['password']))
            {
                return true;
            } else {
                return false;
            }
        }
    }

