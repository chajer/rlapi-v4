<?php
    namespace Ratelimited\Api\Utils;
    use Ratelimited\Api\Models\Users;
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    class Mailer {
        private $dbconn;
        private $mail;

        // Initialization Class
        public function __construct()
        {
            // Initialize DB Connector
            $this->dbconn = pg_connect('host='.getenv('DB_HOST').' port=5432 dbname='.getenv('DB_NAME').' user='.getenv('DB_USERNAME').' password='.getenv('DB_PASSWORD'));

            // Initialize Mailer
            $this->mail = new PHPMailer(true);
            $this->mail->isSMTP();
            $this->mail->Host       =   getenv('SMTP_HOST'); // Set the SMTP server hostname
            $this->mail->Port       =   getenv('SMTP_PORT'); // Set the SMTP server port

            $this->mail->Username   =   getenv('SMTP_USERNAME'); // Set the SMTP login username
            $this->mail->Password   =   getenv('SMTP_PASSWORD'); // Set the SMTP login password

            $this->mail->SMTPAuth   =   true; // The server requires authentication
            $this->mail->SMTPSecure =   PHPMailer::ENCRYPTION_STARTTLS; // Set the encryption as STARTTLS

            $this->mail->setFrom(getenv('MAILER_FROM'), getenv('MAILER_FROM_NAME')); // Set mailer From details
            $this->mail->addReplyTo(getenv('SUPPORT_ADDRESS')); // Set reply-to (Support address)
        }

        public function sendVerificationEmail(string $username, string $email){
            try {
                $this->mail->addAddress($email); // Set the recepient address
                $this->mail->isHTML(true);

                $users                  =   new Users();
                $verification_id        =   $users->generateVerificationLink($email);
                $verification_link      =   getenv('BASE_URI') . '/user/verify/' . $verification_id;

                $this->mail->Subject    =   'Verifying your email on ' . getenv('SERVICE_NAME'); // Set subject
                $this->mail->Body       =   'Hey ' . $username . ',' . PHP_EOL . 'We received a request to sign you up for our services over at ' . getenv('SERVICE_NAME') . '!' . PHP_EOL . 'If this was you, feel free to click the link below and we\'ll activate your account!' . PHP_EOL . 'Verify Here: ' . $verification_link . PHP_EOL . 'Please do note that the link will expire in 24 hours.' . PHP_EOL . 'Thanks,' . PHP_EOL . 'The <b>' . getenv('SERVICE_NAME') . ' Team</b>'; // Set HTML body
                $this->mail->AltBody    =   'Hey ' . $username . ',' . PHP_EOL . 'We received a request to sign you up for our services over at ' . getenv('SERVICE_NAME') . '!' . PHP_EOL . 'If this was you, feel free to click the link below and we\'ll activate your account!' . PHP_EOL . 'Verify Here: ' . $verification_link . PHP_EOL . 'Please do note that the link will expire in 24 hours.' . PHP_EOL . 'Thanks,' . PHP_EOL . 'The ' . getenv('SERVICE_NAME') . ' Team'; // Set plaintext body

                $this->mail->send();
            } catch(\Exception $exception) {
                \Sentry\captureException($exception);

                http_response_code(500); // internal server error
                return [
                    'success' => false,
                    'error_msg' => 'Something went wrong! Please let an instance administrator know.'
                ];
            }
        }
    }