# GET `/user`

GETing the `/user` endpoint while an API key is present in the `Authorization` header will retrieve the currently authenticated user's information.
