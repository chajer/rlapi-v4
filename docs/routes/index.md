# /

The `index` route reports instance information and statistics, such as the version it is running, how many users are on the instance, and the instance contacts.
